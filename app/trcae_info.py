import re
import subprocess
import requests
from collections import namedtuple


def get_trace_info(addr):
    def get_ip_trace():
        command = f"tracert {addr}"
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)

        for line in proc.stdout:
            res = re.findall(r'[0-9]+(?:\.[0-9]+){3}', line.decode())

            if len(res) != 0:
                yield res[0]

    for ind, ip in enumerate(get_ip_trace()):
        if ind == 0:
            continue

        current = namedtuple('IPinfo', 'order_number ip AS_number')

        val = requests.get(f'http://ipinfo.io/{ip}/json', ).json()

        current.order_number = ind
        current.ip = ip

        if val.get('org') is not None:
            current.AS_number = val['org']
        else:
            current.AS_number = 'this ip addr is not white'

        yield current

