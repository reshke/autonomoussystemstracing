import argparse

from app.trcae_info import get_trace_info


def print_table(addr):
    print('-' * 80)

    print('№ ' + ' | ' + 'ip' + ' ' * 13 + ' | ' + 'AS number' + ' ' * 47 + '|')

    def add_with_blanks(num, count):
        str_val = str(num)
        return str_val + ' ' * (count - len(str_val))

    for el in get_trace_info(addr):
        print(f'{add_with_blanks(el.order_number, 2)} | '
              f'{add_with_blanks(el.ip, 15)} | '
              f'{add_with_blanks(el.AS_number, 55)} |')

    print('-' * 80)


def main():
    parser = argparse.ArgumentParser(description='Tracing ip route')
    parser.add_argument('--ip', default=None, type=str,
                        help='IP address to traceroute')
    parser.add_argument('--addr', default=None, type=str,
                        help='address to traceroute')

    args = parser.parse_args()

    addr = args.addr
    ip = args.ip

    print_table(addr) if addr is not None else print_table(ip)


if __name__ == '__main__':
    main()
